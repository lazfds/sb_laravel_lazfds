<?php

class Animal
{

    //protected di sini berfungsi untuk memblokir pemanggilan menggunakan variabel secara langsung
    //Jadi, hanya menggunakan function saja untuk pemanggilan OOP dan hasil nilainya
    //protected juga digunakan untuk menghindari adanya kesalahan pemanggilan variabel di index.php
    //Metode ini menggunakan function saja
    protected $name;
    protected $legs = 4;
    protected $cold_blooded = "no";

    //__construct tidak diubah, tipe function adalah final karena hanya untuk menginisiasikan nama hewan sesuai soal
    //public function bisa di override karena jika sesuai soal tidak harus menginputkan nilai ulang
    final function __construct($nama_hewan)
    {
        $this->name = $nama_hewan;
    }

    public function get_name()
    {
        return $this->name;
    }

    public function get_legs()
    {
        // $this->legs = $jumlah_kaki;
        return $this->legs;
    }

    public function get_type()
    {
        return $this->cold_blooded;
    }
}
