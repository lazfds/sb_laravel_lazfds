<?php

class Frog extends Animal
{
    protected $lompat = "hop hop";
    
    public function jump()
    {
        return $this->lompat;
    }
}