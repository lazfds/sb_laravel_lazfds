<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{

    //function create
    public function index()
    {
        $cast = DB::table('cast')->get(); //select * from cast
        // dd($cast);
        return view('cast.index', compact('cast'));
    }

    //function create
    public function create()
    {
        return view('cast.create');
    }

    //function show
    public function show($id)
    {
        //alternatif : gunakan query get() jika ingin mengambil banyak data
        /*
            //ambil data dari database -> table
            DB::table(nama_table)->where(variable = value)->method
            equivalent to select column from nama_table where variable = value

            //method
            get() : data menjadi collection dan terdiri dari array assosiatif (kumpulan objek)
            first() : data hanya menjadi satu data saja (1 array)
        */
        $cast = DB::table('cast')->where('id', $id)->first();
        // dd($cast);
        return view('cast.show', compact('cast'));
    }

    //function update
    public function update($id, Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
        ]);

        $query = DB::table('cast')
                    ->where('id', $id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio'],
                    ]);

        return redirect('/cast')->with('success', 'Berhasil update data pemeran!');
    }

    //function edit
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }


    //function destroy
    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Berhasil hapus data pemeran!');
    }

    //function store
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio'],
        ]);

        return redirect('/cast')->with('success', 'Data Pemeran berhasil di simpan!');
    }
}
