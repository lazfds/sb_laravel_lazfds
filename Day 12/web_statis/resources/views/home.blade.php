<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="js/bootstrap.min.js">
</head>

<body>
    <!-- header -->
    <header>
        <div class="navigation">
            <nav class="navbar navbar-expand-sm bg-light navbar-light">
                <ul class="navbar-nav">
                    <a class="navbar-brand" href="#">Logo</a>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <!-- content -->
    <article>
        <div class="container">
            <h1>Prima Book</h1>
            <h2>Beli buku lengkap di toko kami!</h2>

            <h3>Kategori Produk</h3>

            <ul>
                <li>Buku Fiksi</li>
                <li>Buku Ilmiah</li>
                <li>Buku Ekonomi & Bisnis</li>
                <li>Buku Hukum</li>
                <li>Hobi</li>
            </ul>

            <h3>Jadilah member Prima Book</h3>
            <ol>
                <li>Kunjungi website kami</li>
                <li>Mendaftar link di <a href="/register">form pendaftaran</a></li>
                <li>Selesai!</li>
            </ol>
        </div>
    </article>


    <!-- footer -->
    <footer>

    </footer>


</body>

</html>
