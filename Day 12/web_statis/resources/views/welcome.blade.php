<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet"  href="css/main.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="js/bootstrap.min.js">
</head>
<body>
    <!-- header -->
    <header>
        <div class="navigation">
            <nav class="navbar navbar-expand-sm bg-light navbar-light">
                <ul class="navbar-nav">
                    <a class="navbar-brand" href="#">Logo</a>
                    <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                    </li>
                </ul>
                </nav>
        </div>
    </header>

    <!-- content -->
    <article>
        <div class="welcome-message">
            <h1>Selamat Datang {{$nama_depan}} {{$nama_belakang}} !</h1>
            <h3>Terima kasih telah bergabung di Prima Book. Beli buku lengkap di toko kami!</h3>
        </div>
    </article>


    <!-- footer -->
    <footer>

    </footer>
</body>
</html>
